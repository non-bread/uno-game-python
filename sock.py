import asyncio
import websockets
import os
import uuid
import pickle
import random
import time
import requests
import websocket
import asyncio
import ast
import json

from random import randint

global all2
global all3
global all4
global ids
global deck
global total

all2=[]
all3=[]
all4=[]

users = []
games = {}
needg=[]
games2=[]    
games3=[]

colors=["1","2","3","4"]
        # 1 =  Red
        # 2 = Blue
        # 3 = Green
        # 4 = Yellow

colors2=["5","6","7","8"]
        # 5 =  Red
        # 6 = Blue
        # 7 = Green
        # 8 = Yellow

cards23=["1","2","3","4","5","6","7","8","9"]

wild=["90"] #Wild card
wild4=["91"] #Wild Draw 4 card 
draw=["6"] #Draw 2
reverse=["7"]
skip=["5"]
# create a global variable for the file name
this_list_file = "thislist.pkl"

# create a function to save the list to the file
def save_list(thislist, filename=None):
    # open the file in write mode
    if filename == None:
        list_file = "thislist.pkl"
    else:
        list_file=filename+".pkl"
   
    with open(list_file, "wb") as f:
        # use pickle to dump the list to the file
        pickle.dump(thislist, f)

# create a function to load the list from the file
def load_list(filename=None):
    # open the file in read mode
    if filename == None:
        list_file = "thislist.pkl"
    else:
        list_file=filename+".pkl"
        
    with open(this_list_file, "rb") as f:
        # use pickle to load the list from the file
        thislist = pickle.load(f)
    # return the list
    return thislist

# tells you what every card is
def what(x):
        colors=["1","2","3","4"]
        # 1 =  Red
        # 2 = Blue
        # 3 = Green
        # 4 = Yellow
    
        colors2=["5","6","7","8"]
        # 5 =  Red
        # 6 = Blue
        # 7 = Green
        # 8 = Yellow
    
        cards=["1","2","3","4","5","6","7","8","9"]
    
        wild=["90"] #Wild card
        wild4=["91"] #Wild Draw 4 card 
        draw=["6"] #Draw 2
        reverse=["7"]
        skip=["5"]
    
        run=0
        total=0
    
    
        for f in range(2):
            for G in range(10):
                if run >= 9:
                    run=-1
                else:
                    for c in range(4):
                        #print("run: "+str(run))
                        color = colors[c]
                        card = cards[run]
                        fine= color + card
                        #print(fine)
                        all2.append(fine)
                        #print(all2)
                        total+=1
                        #print(total)
                        
                run+=1
                
        run=0
        total=0
        for f in range(1):
            for G in range(2):
                if run >= 2:
                    run=-1
                else:
                    for c in range(4):
                        #print("run: "+str(run))
                        color = colors2[c]
                        draw = draw[0]
                        reverse = reverse[0]
                        skip= skip[0]
                        fine= color + draw
                        fine2= color + reverse
                        fine3= color + skip
                        #print(fine)
                        all3.append(fine)
                        all3.append(fine2)
                        all3.append(fine3)
                        #print(all3)
                        total+=1
                        #print(total)
                        
                run+=1
        s=all3
        s = map (int, s) 
        n=all2
        n = map (int, n) 
        intcards = map (int, cards) 

        def get_pos_nums(num):
            pos_nums = []
            while num != 0:
                pos_nums.append(num % 10)
                num = num // 10
            return pos_nums
            
        x1=get_pos_nums(int(x))
        tens=x1[1]
        ones=x1[0]
        anwser=[]
        
        if tens in [5,1]:
            anwser.append("red")
        elif tens in [6,2]:
            anwser.append("Blue")
        elif tens in [7,3]:
            anwser.append("Green")
        elif tens in [8,4]:
            anwser.append("Yellow")
        elif tens in [9]:
            anwser.append("wild")
            
        if ones in intcards:
            if x in s:
                if ones == 7:
                    anwser.append("reverse")
                elif  ones == 6:
                    anwser.append("draw")
                elif ones == 5:
                    anwser.append("skip")
            else:
                if tens == 9:
                    if ones == 1:
                        anwser.append("wild")
                        anwser.append("4")
                    else:
                        anwser.append("wild")
                        anwser.append("0")
                else:
                    anwser.append("n")
                    anwser.append(ones)
        elif ones == 0 or 1:
            if ones == 1:
                anwser.append("wild")
                anwser.append("4")
            else:
                anwser.append("wild")
                anwser.append("0")
        
        return anwser


#random deck
def getdeck():
    colors=["1","2","3","4"]
    # 1 =  Red
    # 2 = Blue
    # 3 = Green
    # 4 = Yellow

    colors2=["5","6","7","8"]
    # 5 =  Red
    # 6 = Blue
    # 7 = Green
    # 8 = Yellow

    cards=["1","2","3","4","5","6","7","8","9"]

    wild=["90"] #Wild card
    wild4=["91"] #Wild Draw 4 card 
    draw=["6"] #Draw 2
    reverse=["7"]
    skip=["5"]

    run=0
    total=0


    for f in range(2):
        for G in range(10):
            if run >= 9:
                run=-1
            else:
                for c in range(4):
                    #print("run: "+str(run))
                    color = colors[c]
                    card = cards[run]
                    fine= color + card
                    #print(fine)
                    all2.append(fine)
                    #print(all2)
                    total+=1
                    #print(total)
                    
            run+=1
            
    run=0
    total=0
    for f in range(1):
        for G in range(2):
            if run >= 2:
                run=-1
            else:
                for c in range(4):
                    #print("run: "+str(run))
                    color = colors2[c]
                    draw = draw[0]
                    reverse = reverse[0]
                    skip= skip[0]
                    fine= color + draw
                    fine2= color + reverse
                    fine3= color + skip
                    #print(fine)
                    all3.append(fine)
                    all3.append(fine2)
                    all3.append(fine3)
                    #print(all3)
                    total+=1
                    #print(total)
                    
            run+=1

    #add 4 of each wild
    for i in range(4):
        all4.append(wild[0])
        all4.append(wild4[0])
        #print(all4)  
              
    #combines the deck
    allt=all2 + all3 + all4
    deck = [eval(i) for i in allt]

    #randomizes the deck
    t = time.localtime()
    secs = 3600 * t.tm_hour + 60 * t.tm_min + t.tm_sec
    random.Random(secs).shuffle(deck)
    #print(deck)
    return deck
   
def create_game(user1, user2):
    game_id = str(uuid.uuid4())
    games[game_id] = [user1, user2]
    return game_id
    
def deal_cards():
    deck=[]
    deck = getdeck()
    random.shuffle(deck)
    i=0
    deck1=[]
    while i < 7:
        num = random.randint(0,(len(deck)))
        outout = deck.pop(num)
        deck1.append(outout)

        i+=1
    i=0
    deck2=[]
    while i < 7:
        num = random.randint(0,(len(deck)))
        outout = deck.pop(num)
        deck2.append(outout)

        i+=1
    return deck1, deck2, deck

async def handler(websocket, path):
    async for message in websocket:
        if path == '/test':
            data = message
            reply = f"Data recieved as:  {data}!"
            await websocket.send(reply)
        elif path == '/mwhat':
            mess=what(int(message))
            await websocket.send(str(mess))
        elif path == '/bruh2':
            pecards=ast.literal_eval(message)
            pecards=ast.literal_eval(pecards[0])
            print(pecards)
            pecards2=""
            for i in range(len(pecards)):
                convert=what(int(pecards[i]))
                pecards2+=str(convert)+"  "+str(i)+"""
            """
            await websocket.send(pecards2)
        #check if user's game is found
        elif path == '/cgame':
            if message in needg:
                await websocket.send('3')
            else:
                i=0
                for i in range(0, len(games2)):
                    idk=games2[i]
                    idk=ast.literal_eval(idk)
                    print(idk)
                    ug=idk[3]
                    if message == ug or ug in message:
                        game_id=idk[0]
                        player2_cards=idk[4]
                        pturn=idk[6]
                        startc=idk[8]
                        
                        await websocket.send(f"['{game_id}','{player2_cards}', '{startc}']")
        elif path == '/mturn':
            if message in needg:
                await websocket.send(f"['3']")
            else:
                i=0
                for i in range(0, len(games2)):
                    idk=games2[i]
                    idk=ast.literal_eval(idk)
                    print(idk)
                    ug=idk[6]
                    card=idk[8]
                    if message == idk[3]:
                        dekc=idk[4]
                    else:
                        dekc=idk[2]
                    
                    #print("ug: "+ug)
                    #print(message)
                    if message == ug:
                        await websocket.send(f"['2', '{card}', '{dekc}']")
                    else:
                        await websocket.send(f"['1']")
        elif path == '/play':
            id4=ast.literal_eval(message)
            print(message)
            print(needg)
            print(id4)
            if message[0] in needg:
                await websocket.send(f"['3']")
            else:
                i=0
                for i in range(0, len(games2)):
                    idk=games2[i]
                    idk=ast.literal_eval(idk)
                    bruhdd=i
                    print(idk)
                    ug=idk[6]
                    ppm=id4[0]
                    print("ug: "+str(ug))
                    print("id4[0]:"+str(id4[0]))
                    print("id4:"+str(id4))

                    print(str(ppm))
                    print(str(ug))
                    if  str(ppm) == str(ug) or str(id4[0]) == str(idk[6]):
                        startc=idk[8]
                        cards=id4[1]  
                        cards=ast.literal_eval(cards)
                        print(cards)
                        out=cards.pop(int(id4[2]))
                        g=what(out)
                        print(g)
                        c=what(startc)
                        print(c)
                        if 3 <= len(c):
                            pass
                        else:
                            c.insert(2, "j")
                        if 3 <= len(g):
                            pass
                        else:
                            g.insert(2, "k")
                        print("g (out): "+str(g))
                        print("c (startc): "+str(c))
                        if c[0] == g[0]:
                                if idk[6] == idk[1]:
                                   p1cards=ast.literal_eval(idk[2])
                                   del p1cards[int(id4[2])]
                                   idk[2]=f"{p1cards}"
                                elif idk[6] == idk[3]:
                                   p1cards=ast.literal_eval(idk[4])
                                   del p1cards[int(id4[2])]
                                   idk[4]=f"{p1cards}"
                                   
                                   
                                idk[8]=out
                                if idk[6] == idk[3]:
                                    idk[6]=idk[1]
                                else:
                                    idk[6]=idk[3]
                                games2[int(bruhdd)]=f"{idk}"
                                print(games2[int(bruhdd)])
                                await websocket.send(f"{out}")
                        elif g[1] == "skip":
                                usersid=id4[0]
                                if idk[6] == idk[1]:
                                   p1cards=ast.literal_eval(idk[2])
                                   del p1cards[int(id4[2])]
                                   idk[2]=f"{p1cards}"
                                elif idk[6] == idk[3]:
                                   p1cards=ast.literal_eval(idk[4])
                                   del p1cards[int(id4[2])]
                                   idk[4]=f"{p1cards}"
                                   
                                if idk[1] == idk[6]:
                                    next_player_index = (idk.index(idk[6]) + 1) % 1  # Cycle through player IDs
                                    idk[6] = idk[next_player_index * 2 + 1]                                      
                                    print("idk[6]: "+str(idk[6]))
                                
                                idk[8]=out
                                games2[int(bruhdd)]=f"{idk}"
                                print(games2[int(bruhdd)])
                                await websocket.send(f"{out}")
                             
                        elif g[1] == "wild":                        
                                if idk[6] == idk[1]:
                                   p1cards=ast.literal_eval(idk[2])
                                   del p1cards[int(id4[2])]
                                   idk[2]=f"{p1cards}"
                                elif idk[6] == idk[3]:
                                   p1cards=ast.literal_eval(idk[4])
                                   del p1cards[int(id4[2])]
                                   idk[4]=f"{p1cards}"
                                   
                                cardgened=(int(colors[int(randint(0, len(colors)))])*10) + int(cards23[int(randint(0, len(cards23)))])
                                idk[8]=cardgened
                                if idk[6] == idk[3]:
                                    idk[6]=idk[1]
                                else:
                                    idk[6]=idk[3]
                                if g[2] == "4":
                                    bruhs=ast.literal_eval(idk[5])
                                    num = random.randint(0,(len(bruhs)))
                                    outout = bruhs.pop(num)
                                    num = random.randint(0,(len(bruhs)))
                                    outout2 = bruhs.pop(num)
                                    idk[5]=str(bruhs)
                                    p=idk[6]
                                    
                                    if p != idk[1]:
                                        bruh=ast.literal_eval(idk[2])
                                        print(bruh)
                                        bruh.append(outout)
                                        bruh.append(outout2)
                                        idk[2]=f'{bruh}'
                                    else:
                                        bruh=ast.literal_eval(idk[4])
                                        print(bruh)
                                        bruh.append(outout)
                                        bruh.append(outout2)
                                        idk[4]=str(bruh)
                                        
                                games2[int(bruhdd)]=f"{idk}"
                                print(games2[int(bruhdd)])
                                await websocket.send(f"{out}")
                        elif g[1] == "reverse":   
                            if g[1] == c[1] or c[0]==g[0]:
                                await websocket.send(f"this card does nothing")
                            else:
                                pcard=0
                                card3=[]
                                k=what(idk[8])
                                for i in range(len(cards)):
                                    card=cards[i]
                                    b=what(card)
                                    card3.append(b)
                                print("card3:"+str(card3))
                                for i in card3:
                                    if i[0] == k[0]:
                                        pcard=1
                                    elif i[1] == k[2]:
                                        pcard=1
                                    elif i[2] == k[2]:
                                        pcard=1
                                #if k[0] or k[2] in card3:
                                 #   pcard=1
                                await websocket.send(f"{out}")
                                    
                                if pcard == 1:                            
                                    await websocket.send(f"['2','wrong card']")
                                else:
                                    bruhs=ast.literal_eval(idk[5])
                                    num = random.randint(0,(len(bruhs)))
                                    outout = bruhs.pop(num)
                                    idk[5]=str(bruhs)
                                    p=idk[6]
                                    
                                    if p == idk[1]:
                                        bruh=ast.literal_eval(idk[2])
                                        print(bruh)
                                        bruh.append(outout)
                                        idk[2]=f'{bruh}'
                                    else:
                                        bruh=ast.literal_eval(idk[4])
                                        print(bruh)
                                        bruh.append(outout)
                                        idk[4]=str(bruh)
                                    games2[int(bruhdd)]=f"{idk}"
                                    await websocket.send(f"['4','{outout}','card isn't in deck']")   
                        elif g[1] == "draw":
                            # Check if the played card is also a draw card
                            if c[1] == g[1]:
                                bruhs = ast.literal_eval(idk[5])  # remaining_deck

                                # Draw two cards, ensuring deck replenishment if needed
                                for _ in range(2):
                                    if len(bruhs) == 0:  # Deck is empty, refill
                                        discard_pile = ast.literal_eval(idk[8])  # Get discard pile
                                        bruhs = discard_pile[:-1]  # Exclude top card
                                        random.shuffle(bruhs)  # Shuffle discard pile
                                        idk[8] = discard_pile[-1]  # Keep top card as discard pile

                                    num = random.randint(0, (len(bruhs)))
                                    outout = bruhs.pop(num)
                                    idk[5] = str(bruhs)  # Update remaining_deck

                                    # Add drawn card to the correct player's hand
                                    p = idk[6]  # Current player's ID
                                    if p == idk[1]:  # Player 1
                                        bruh = ast.literal_eval(idk[2])
                                        bruh.append(outout)
                                        idk[2] = f'{bruh}'
                                    else:  # Player 2
                                        bruh = ast.literal_eval(idk[4])
                                        bruh.append(outout)
                                        idk[4] = f'{bruh}'

                                games2[int(bruhdd)] = f"{idk}"
                                await websocket.send(f"{out}")

                            else:
                                    print("c0:"+str(c[0]))
                                    print("g0"+str(g[0]))
                                    print("c1:"+str(c[1]))
                                    print("g1:"+str(g[1]))
                                    pcard=0
                                    card3=[]
                                    k=what(idk[8])
                                    for i in range(len(cards)):
                                        card=cards[i]
                                        b=what(card)
                                        card3.append(b)
                                    print("card3:"+str(card3))
                                    for i in card3:
                                        if i[0] == k[0]:
                                            pcard=1
                                        elif i[1] == k[2]:
                                            pcard=1
                                        if len(i) == 3 and len(k) == 3:
                                            if i[2] == k[2]:
                                                pcard=1
                                    #if k[0] or k[2] in card3:
                                     #   pcard=1
                                        
                                    if pcard == 1:                            
                                        await websocket.send(f"['9','bread: 25']")
                                    else:
                                        bruhs=ast.literal_eval(idk[5])
                                        num = random.randint(0,(len(bruhs)))
                                        outout = bruhs.pop(num)
                                        idk[5]=str(bruhs)
                                        p=idk[6]
                                        
                                        if p == idk[1]:
                                            bruh=ast.literal_eval(idk[2])
                                            print(bruh)
                                            bruh.append(outout)
                                            idk[2]=f'{bruh}'
                                        else:
                                            bruh=ast.literal_eval(idk[4])
                                            print(bruh)
                                            bruh.append(outout)
                                            idk[4]=str(bruh)
                                        games2[int(bruhdd)]=f"{idk}"
                                        await websocket.send(f"['4','{outout}','card isn't in deck']")
                            #elif g[1] == 
                        elif (g[1] == "n") == (c[0] == g[0] or c[2] == g[2]):
                                if idk[6] == idk[1]:
                                   p1cards=ast.literal_eval(idk[2])
                                   del p1cards[int(id4[2])]
                                   idk[2]=f"{p1cards}"
                                elif idk[6] == idk[3]:
                                   p1cards=ast.literal_eval(idk[4])
                                   del p1cards[int(id4[2])]
                                   idk[4]=f"{p1cards}"
                                   
                                idk[8]=out
                                if idk[6] == idk[3]:
                                    idk[6]=idk[1]
                                else:
                                    idk[6]=idk[3]
                                games2[int(bruhdd)]=f"{idk}"
                                print(games2[int(bruhdd)])
                                await websocket.send(f"{out}")
                        else:
                            pcard=0
                            card3=[]
                            k=what(idk[8])
                            for i in range(len(cards)):
                                card=cards[i]
                                b=what(card)
                                card3.append(b)
                            print("card3:"+str(card3))
                            for i in card3:
                                if i[0] == k[0]:
                                    pcard=1
                                elif i[1] == k[2]:
                                    pcard=1
                                if len(i) == 3 and len(k) == 3:
                                    if i[2] == k[2]:
                                        pcard=1
                            #if k[0] or k[2] in card3:
                             #   pcard=1
                                
                            if pcard == 1:                            
                                await websocket.send(f"['2','wrong card']")
                            else:
                                bruhs=ast.literal_eval(idk[5])
                                num = random.randint(0,(len(bruhs)))
                                outout = bruhs.pop(num)
                                idk[5]=str(bruhs)
                                p=idk[6]
                                
                                if p == idk[1]:
                                    bruh=ast.literal_eval(idk[2])
                                    print(bruh)
                                    bruh.append(outout)
                                    idk[2]=f'{bruh}'
                                else:
                                    bruh=ast.literal_eval(idk[4])
                                    print(bruh)
                                    bruh.append(outout)
                                    idk[4]=str(bruh)
                                games2[int(bruhdd)]=f"{idk}"
                                await websocket.send(f"['4','{outout}','card isn't in deck']")
                    else:
                        print("*********************bruh nice try sent here*********************")
                        await websocket.send(f"['1','bruh nice try']")
        elif path == '/mgame':
            user_id = message
            if len(needg) >= 1:
                user1 = user_id
                try:
                    user2 = needg.pop(0)
                except:
                    user2=''
                if user2 == '':
                    if user_id in users:
                        users.remove(user_id)
                        
                    if user_id not in needg:
                        needg.append(user_id)
                else:
                    game_id = create_game(user1, user2)
                    player1_cards, player2_cards, remaining_deck = deal_cards()
                    pturn=user1
                    pstyle="1"
                    startc=remaining_deck.pop(3)
                    gamefo=f"['{game_id}','{user1}','{player1_cards}','{user2}','{player2_cards}','{remaining_deck}', '{pturn}', '{pstyle}', '{startc}']"
                    games2.append(gamefo)
                    if user2 == user_id:
                        await websocket.send(f"['{game_id}','{player2_cards}', '{startc}']")
                    elif user1 == user_id:
                        await websocket.send(f"['{game_id}','{player1_cards}', '{startc}']")
                    else:
                        pass
            else:
                await websocket.send("Waiting for another player...")
                if user_id in users:
                    users.remove(user_id)
                    
                if user_id not in needg:
                    needg.append(user_id)

        elif path == "/disconnect":
            user_id = message
            if user_id in users:
                users.remove(user_id)
        elif path == "/send":
            number = message
            try:
                thislist = load_list()
            except:
                thislist = []
            if number == '2':
                ids=uuid.uuid4()
                ids=ids
                thislist.insert((1+len(thislist)), str(ids))
                save_list(thislist)
                await websocket.send(str(ids))
            else:
                await websocket.send("false")
        elif path == "/disconnect":
            # try to load the list from the file, or create a new one if it fails
            thislist = load_list()
            print(thislist)
            data = message
            user_id  = data
            thislist.remove(user_id)
            save_list(thislist)
            print(thislist)
            print(user_id )
        else:
            print(f"Unhandled path: {path}")

 
async def main():
    async with websockets.serve(handler, "127.0.0.1", 465,ping_interval=None):
        await asyncio.Future()  # run forever
print('started')
asyncio.run(main())
